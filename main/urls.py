from django.conf.urls import url
from .views import *



urlpatterns=[
    #url(r'^projectnew/$', views.create_projec,),
    #url(r'^projectview/$', views.list_project,),
    #url(r'^projectdetail/(?P<idp>[0-9]+)/$', views.detail_project, ),
    url(r'^$', ProjectIndex.as_view(), name='project_index'),
    url(r'^project_index$', ProjectIndex.as_view(), name='project_index'),
    url(r'^project_add$', ProjectAdd.as_view(), name="project_add"),
    url(r'^(?P<pk>\d+)/project_view/$', ProjectView.as_view(), name="project_view"),
    url(r'^(?P<pk>\d+)/project_edit/$', ProjectEdit.as_view(), name="project_edit"),
    url(r'^(?P<pk>\d+)/project_delete/$', ProjectDelete.as_view(), name="project_delete"),
]
from django.db import models

# Create your models here.

Type_of_user=(('Testing','Testing'),
                  ('Administrator','Administrator'))

Status_of_projects=(('Active','Active'),
                    ('inactive','inactive'),
                                       )

class User(models.Model):
    id=models.AutoField(primary_key=True, null=False, unique=True)
    name=models.CharField(null=False,max_length=50)
    last_name=models.CharField(null=False,max_length=50)
    nickname=models.CharField(null=False,max_length=50)
    status=models.BooleanField(null=False, default=False)
    type=models.CharField(choices=Type_of_user, null=False, max_length=50)

    def __str__(self):
        return '%s'% (self.nickname)
    def __getnamec__(self):
        return '%s %s' %(self.name,self.last_name)



class FilesOfp(models.Model):
    file=models.CharField(max_length=300)
    def __str__(self):
        return '%s-%s'% (self.id,self.file)







class Project(models.Model):
    idp=models.AutoField(primary_key=True, null=False, unique=True)
    name=models.CharField(null=False, max_length=120)
    URL=models.URLField(null=False)
    creation_date=models.DateField(auto_now_add=True)
    status=models.CharField(null=False,max_length=60, choices=Status_of_projects)
    fle=models.ManyToManyField(FilesOfp)
    id=models.ForeignKey(User)


    def __str__(self):
        return '%s   -     %s    -    %s    -    %s' % (self.name, self.creation_date,self.status,self.id)











class Rulez(models.Model):
    idre=models.AutoField(primary_key=True, null=False, unique=True)
    Typer=models.CharField(null=False, max_length=150)
    description=models.CharField(null=False, max_length=300)
    codigo=models.CharField(null=False, max_length=40)






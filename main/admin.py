from django.contrib import admin
from main.models import *

# Register your models here.

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name','creation_date','status','id')

admin.site.register(Project, ProjectAdmin)
admin.site.register(User)
admin.site.register(Rulez)
admin.site.register(FilesOfp)
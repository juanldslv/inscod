# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilesOfp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('file', models.CharField(max_length=300)),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='status',
            field=models.CharField(max_length=60, choices=[('Active', 'Active'), ('inactive', 'inactive')]),
        ),
        migrations.AddField(
            model_name='project',
            name='fle',
            field=models.ManyToManyField(to='main.FilesOfp'),
        ),
    ]

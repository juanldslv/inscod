# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('idp', models.AutoField(primary_key=True, unique=True, serialize=False)),
                ('name', models.CharField(max_length=120)),
                ('URL', models.URLField()),
                ('creation_date', models.DateField(auto_now_add=True)),
                ('status', models.CharField(max_length=60, choices=[('queued', 'queued'), ('finished', 'finished'), ('progress', 'progress')])),
            ],
        ),
        migrations.CreateModel(
            name='Rulez',
            fields=[
                ('idre', models.AutoField(primary_key=True, unique=True, serialize=False)),
                ('Typer', models.CharField(max_length=150)),
                ('description', models.CharField(max_length=300)),
                ('codigo', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(primary_key=True, unique=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('nickname', models.CharField(max_length=50)),
                ('status', models.BooleanField(default=False)),
                ('type', models.CharField(max_length=50, choices=[('Testing', 'Testing'), ('Administrator', 'Administrator')])),
            ],
        ),
        migrations.AddField(
            model_name='project',
            name='id',
            field=models.ForeignKey(to='main.User'),
        ),
    ]

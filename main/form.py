from django import forms
from .models import *


class ProjectForm(forms.ModelForm):
    fle=forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=FilesOfp.objects.all())
    class Meta:

        model=Project
        fields=('name','URL','status','id','fle',)
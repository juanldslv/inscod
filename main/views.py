from datetime import timezone

from django.shortcuts import render, render_to_response, redirect
from main.models import *
from main.form import *
from django.http import Http404
import requests
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# Create your views here.

class ProjectIndex(View):

    template_name='index.html'


    def get(self,request, *args, **kwargs):
        projects=Project.objects.filter(status='Active')
        return render(request, self.template_name, {'projects':projects,})
        print(projects)


class ProjectAdd(View):
    template_name='add.html'

    def get(self, request, *args, **kwargs):
        form = ProjectForm()
        return render(request, self.template_name, {
                    'form': form,
                            })

    def post(self, request, *args, **kwargs):
        form = ProjectForm(request.POST)
        if form.is_valid():
            projects = form.save()
            return HttpResponseRedirect(reverse('main:project_index'))
        else:
            return render(request, self.template_name, {'form': form})

class ProjectEdit(View):
    template_name = "edit.html"

    def get(self, request, *args, **kwargs):
            project = Project.objects.get(pk=kwargs['pk'])
            form = ProjectForm(instance=project)
            return render(request, self.template_name, {
            'form': form,
            })
    def post(self, request, *args, **kwargs):
            project = Project.objects.get(pk=kwargs['pk'])
            form = ProjectForm(request.POST, instance=project)
            if form.is_valid():
                project = form.save()
                return HttpResponseRedirect(reverse('main:project_index'))
            else:
                return render(request, self.template_name, {'form': form, })


class ProjectView(View):
    template_name='view.html'
#aquí mostrar el repositorio

    def get(self,request, *args, **kwargs):
        project=Project.objects.get(pk=kwargs['pk'])
        return render(request, self.template_name, {'project': project})


class ProjectDelete(View):
   def get(self, request, *args, **kwargs):
        account = Project.objects.get(pk=kwargs['pk'])
        account.deleted = True
        account.save()
        return HttpResponseRedirect(reverse('inspector:project_index'))